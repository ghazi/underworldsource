# The Original Underworld Source Code #

## Note: Now supports Petsc 3.7! ##


Underworld1 is a parallel, particle-in-cell finite element code for large-scale geodynamics simulations. This repository is an attempt to keep alive the original source code for Underworld (now Underworld1) prior to the development of Underworld2. The source code has been obtained from the underworldproject repositories on github and modified from there.

## 1. Dependencies ##

  - libXML2
  - MPI
  - PETSc
  - HDF5

For visualisation (optional):

  - X11 ( for desktop machines ) or OSMesa (for cluster or machines
    without displays) or SDL
  - libPNG or libJPEG
  - libFAME or libavcodec

The main troublesome dependencies I will list below:

## 2. Modules

Loading modules should work on most of the modern machines:

module load openmpi
module load petsc
module load hdf5

If any of those don't work, see the following: 

### HDF5

Compile HDF5 with multithread support

### PETSC (including MPI and HDF5) ###

Compiling PETSC can be tricky (especially with the intel compiler). You will need to compile it with hdf5 and mpi support.

#### Build using GNU Compiler (Intel follows below)

then create variables PETSC_DIR and PETSC_ARCH (put them in the bash .profile file for ease), e.g.

```
export PETSC_DIR=$HOME/Code/petsc
export PETSC_ARCH=linux-openmpi.gnu
```

don't forget to
```
source ~/.profile
```

I use the following configure settings:

```
# Configure petsc with gnu compiler (use an install directory, e.g. $HOME/Programs/petsc)

./configure --download-fblaslapack=yes --prefix=$HOME/Programs/petsc
```

Follow the make instructions that come after the build, e.g.

```
make PETSC_DIR=$PETSC_DIR PETSC_ARCH=linux-openmpi.gnu all
make PETSC_DIR=$PETSC_DIR PETSC_ARCH=linux-openmpi.gnu install
```

#### Intel compilers

```
export PETSC_ARCH=linux-openmpi-1.8.3_intel for the intel compiler version
module load hdf5/1.8.3_intel
./configure --download-fblaslapack=yes --with-mpi-dir=/cluster/software/VERSIONS/openmpi.intel-1.8.3 --download-sowing --download-sowing-cc=mpicc --download-sowing-cxx=mpicxx
```

For some reason mpicc is picked out by petsc, but not for the sowing compile, so the sowing compilers also need to be added

To debug sowing, go to the sowing directory under petsc (which has now been downloaded and created)

```
cd $PETSC_DIR/$PETSC_ARCH/externalpackages/git.sowing
```

and run
```
./configure --enable-strict --datadir=`pwd`/share --prefix=`pwd` CC=mpicc CXX=mpicxx
```
and then pass those CC, CXX variables to the download-sowing-cc etc. variables in the main configuration utility in petsc


## 2. Configuration of Underworld##

To configure:
```
  ./configure.py --petsc-dir=$HOME/Programs/petsc
```
Available options can be listed with:
```
  ./configure.py --help
```
If this does not work, the config.cfg file can be modified directly. I include a sample version in the source code for reference

 use

```
./configure.py --prefix=dir to install to local directory dir
```

## 3. Build ##

To build (note this reads config.cfg, which can be modified by hand):
```
  ./scons.py
```
To rebuild as fast as possible (note that this is not guaraunteed to produce a consistent build):
```
  ./fast-scons.py
```
To run tests:
```
# Runs unit-tests & low resolution integration tests
  ./scons.py check
```
## 4. Install

If an install location has been specified at the configuration stage
(using the 'prefix' option), you may finally install Underworld to the
provided location using the install option:
```
  ./scons.py install
```
## 5. Known Issues ##

* On a MAC or when building static builds or when using icc/pgi
  compilers, significant warnings are produced during compilation,
  don't be alarmed, we are working on them.
* Old Underworld input files prior to the 1.4.0 release are not
  compatible to 1.4.0 and above releases.   

## 6. Credits ##

Credit to the original VPAC/Monash team for the development of the software. 

## 7. Documentation ##

The user manual may be found at Docs/Manual/Underworld.pdf
