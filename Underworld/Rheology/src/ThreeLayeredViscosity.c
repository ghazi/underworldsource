/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
** Copyright (c) 2005-2010, Monash University
** All rights reserved.
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
**       * Redistributions of source code must retain the above copyright notice,
**          this list of conditions and the following disclaimer.
**       * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**       * Neither the name of the Monash University nor the names of its contributors
**       may be used to endorse or promote products derived from this software
**       without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
** BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
** CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
** SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
** OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
**
** Contact:
*%  Louis.Moresi - Louis.Moresi@monash.edu
*%
*% Development Team :
*%  http://www.underworldproject.org/aboutus.html
**
**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include <mpi.h>
#include <StGermain/StGermain.h>
#include <StgDomain/StgDomain.h>
#include <StgFEM/StgFEM.h>
#include <PICellerator/PICellerator.h>

#include "types.h"
#include "RheologyClass.h"
#include "ConstitutiveMatrix.h"
#include "ThreeLayeredViscosity.h"

#include <assert.h>
#include <string.h>

/* Textual name of this class - This is a global pointer which is used for times when you need to refer to class and not a particular instance of a class */
const Type ThreeLayeredViscosity_Type = "ThreeLayeredViscosity";

/* Public Constructor */
ThreeLayeredViscosity* ThreeLayeredViscosity_New(
      Name                  name,
      AbstractContext*      context,
      FeMesh*               feMesh,
      double                eta0_1,
      double                eta0_2,
      double                eta0_3,
      Axis                  variationAxis,
      double                referencePoint1,
      double                referencePoint2)
{
   ThreeLayeredViscosity* self = (ThreeLayeredViscosity*) _ThreeLayeredViscosity_DefaultNew( name );

   _Rheology_Init( self, (PICelleratorContext*)context );
   _ThreeLayeredViscosity_Init( self, feMesh, eta0_1, eta0_2,eta0_3, variationAxis, referencePoint1, referencePoint2 );
   self->isConstructed = True;
   return self;
}

/* Private Constructor: This will accept all the virtual functions for this class as arguments. */
ThreeLayeredViscosity* _ThreeLayeredViscosity_New(  THREELAYEREDVISCOSITY_DEFARGS  )
{
   ThreeLayeredViscosity*               self;

   /* Call private constructor of parent - this will set virtual functions of parent and continue up the hierarchy tree. At the beginning of the tree it will allocate memory of the size of object and initialise all the memory to zero. */
   assert( _sizeOfSelf >= sizeof(ThreeLayeredViscosity) );
   self = (ThreeLayeredViscosity*) _Rheology_New(  RHEOLOGY_PASSARGS  );

   return self;
}

void _ThreeLayeredViscosity_Init( ThreeLayeredViscosity* self, FeMesh* feMesh, double eta0_1, double eta0_2, double eta0_3, Axis variationAxis, double referencePoint1, double referencePoint2 ) {
   self->feMesh          = feMesh;
   self->eta0_1            = eta0_1;
   self->eta0_2            = eta0_2;
   self->eta0_3            = eta0_3;
   self->variationAxis   = variationAxis;
   self->referencePoint1  = referencePoint1;
   self->referencePoint2  = referencePoint2;
}

void* _ThreeLayeredViscosity_DefaultNew( Name name ) {
	/* Variables set in this function */
	SizeT                                                     _sizeOfSelf = sizeof(ThreeLayeredViscosity);
	Type                                                             type = ThreeLayeredViscosity_Type;
	Stg_Class_DeleteFunction*                                     _delete = _Rheology_Delete;
	Stg_Class_PrintFunction*                                       _print = _Rheology_Print;
	Stg_Class_CopyFunction*                                         _copy = _Rheology_Copy;
	Stg_Component_DefaultConstructorFunction*         _defaultConstructor = _ThreeLayeredViscosity_DefaultNew;
	Stg_Component_ConstructFunction*                           _construct = _ThreeLayeredViscosity_AssignFromXML;
	Stg_Component_BuildFunction*                                   _build = _Rheology_Build;
	Stg_Component_InitialiseFunction*                         _initialise = _Rheology_Initialise;
	Stg_Component_ExecuteFunction*                               _execute = _Rheology_Execute;
	Stg_Component_DestroyFunction*                               _destroy = _ThreeLayeredViscosity_Destroy;
	Rheology_ModifyConstitutiveMatrixFunction*  _modifyConstitutiveMatrix = _ThreeLayeredViscosity_ModifyConstitutiveMatrix;

	/* Variables that are set to ZERO are variables that will be set either by the current _New function or another parent _New function further up the hierachy */
	AllocationType  nameAllocationType = NON_GLOBAL /* default value NON_GLOBAL */;

   return (void*) _ThreeLayeredViscosity_New(  THREELAYEREDVISCOSITY_PASSARGS  );
}

void _ThreeLayeredViscosity_AssignFromXML( void* rheology, Stg_ComponentFactory* cf, void* data ){
   ThreeLayeredViscosity*  self                   = (ThreeLayeredViscosity*)rheology;
   FeMesh*          feMesh;
   Axis                      variationAxis          = 0;
   Name                      variationAxisName;
   Stream*                   errorStream            = Journal_Register( Error_Type, (Name)self->type  );

   /* Construct Parent */
   _Rheology_AssignFromXML( self, cf, data );

   feMesh = Stg_ComponentFactory_ConstructByKey( cf, self->name, (Dictionary_Entry_Key)"Mesh", FeMesh, True, data  ) ;

   variationAxisName = Stg_ComponentFactory_GetString( cf, self->name, (Dictionary_Entry_Key)"variationAxis", "Y" );

   Journal_Firewall(
         variationAxisName != NULL && strlen( variationAxisName  ) >= 1,
         errorStream,
         "Error in func %s for %s '%s' - Bad 'variationAxis'\n",
         __func__, self->type, self->name );

   switch ( variationAxisName[0] ) {
      case 'X': case 'x': case 'I': case 'i': case '0':
         variationAxis = I_AXIS; break;
      case 'Y': case 'y': case 'J': case 'j': case '1':
         variationAxis = J_AXIS; break;
      case 'Z': case 'z': case 'K': case 'k': case '2':
         variationAxis = K_AXIS; break;
      default:
         Journal_Firewall(
            False,
            errorStream,
            "Error in func %s for %s '%s' - Bad 'variationAxis' in dictionary.\n",
            __func__, self->type, self->name );
   }

   _ThreeLayeredViscosity_Init(
         self,
         feMesh,
         Stg_ComponentFactory_GetDouble( cf, self->name, (Dictionary_Entry_Key)"eta0_1", 1.0  ),
         Stg_ComponentFactory_GetDouble( cf, self->name, (Dictionary_Entry_Key)"eta0_2", 1.0  ),
         Stg_ComponentFactory_GetDouble( cf, self->name, (Dictionary_Entry_Key)"eta0_3", 1.0  ),
         variationAxis,
         Stg_ComponentFactory_GetDouble( cf, self->name, (Dictionary_Entry_Key)"referencePoint1", 0.0 ), 
         Stg_ComponentFactory_GetDouble( cf, self->name, (Dictionary_Entry_Key)"referencePoint2", 0.0 )  );
}

void _ThreeLayeredViscosity_Destroy( void* rheology, void* data ) {
	ThreeLayeredViscosity* self = (ThreeLayeredViscosity*) rheology;

	Stg_Component_Destroy( self->feMesh, data, False );
	/* Destroy parent */
	_Rheology_Destroy( self, data );

}


void _ThreeLayeredViscosity_ModifyConstitutiveMatrix(
      void*                                              rheology,
      ConstitutiveMatrix*                                constitutiveMatrix,
      MaterialPointsSwarm*                               swarm,
      Element_LocalIndex                                 lElement_I,
      MaterialPoint*                                     materialPoint,
      Coord                                              xi )
{
   ThreeLayeredViscosity*     self              = (ThreeLayeredViscosity*) rheology;
   double                            depth;
   double                            viscosity;
   Coord                             coord;

   /* This rheology assumes particle is an integration points thats can be mapped to a particle
    * that has no meaningful coord. Best to re-calc the global from local */

   /* Calculate distance */
   FeMesh_CoordLocalToGlobal( ((IntegrationPointsSwarm*)constitutiveMatrix->integrationSwarm)->mesh, lElement_I, xi, coord );
   depth = coord[ self->variationAxis ];
   /* Calculate New Viscosity */
	if (depth <= self->referencePoint1) {
		viscosity = self->eta0_1;
	}
	else if (depth >= self->referencePoint2) {
		viscosity = self->eta0_3;
	}
	else {
		viscosity = self->eta0_2;
	}
   ConstitutiveMatrix_SetIsotropicViscosity( constitutiveMatrix, viscosity );
}


