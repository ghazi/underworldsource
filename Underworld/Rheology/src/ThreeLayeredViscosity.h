

#ifndef __Underworld_Rheology_ThreeLayeredViscosity_h__
#define __Underworld_Rheology_ThreeLayeredViscosity_h__

	/** Textual name of this class - This is a global pointer which is used for times when you need to refer to class and not a particular instance of a class */
	extern const Type ThreeLayeredViscosity_Type;

	/** Rheology class contents - this is defined as a macro so that sub-classes of this class can use this macro at the start of the definition of their struct */
	#define __ThreeLayeredViscosity \
		/* Macro defining parent goes here - This means you can cast this class as its parent */ \
		__Rheology \
		/* Virtual functions go here */ \
		/* Material Parameters */\
		FeMesh*    		                            feMesh;                             \
		double                                              eta0_1;                               \
		double                                              eta0_2;                               \
		double                                              eta0_3;                               \
		Axis                                                variationAxis;                      \
		double                                              referencePoint1;                     \
		double                                              referencePoint2;                     \
		double                                              referencePoint3;                     

	struct ThreeLayeredViscosity { __ThreeLayeredViscosity };

	/** Public Constructor */
   ThreeLayeredViscosity* ThreeLayeredViscosity_New(
      Name                  name,
      AbstractContext*      context,
      FeMesh*               feMesh,
      double                eta0_1,
      double                eta0_2,
      double                eta0_3,
      Axis                  variationAxis,
      double                referencePoint1, 
      double                referencePoint2);
	
	/** Private Constructor: This will accept all the virtual functions for this class as arguments. */
	
	#ifndef ZERO
	#define ZERO 0
	#endif

	#define THREELAYEREDVISCOSITY_DEFARGS \
                RHEOLOGY_DEFARGS

	#define THREELAYEREDVISCOSITY_PASSARGS \
                RHEOLOGY_PASSARGS

	ThreeLayeredViscosity* _ThreeLayeredViscosity_New(  THREELAYEREDVISCOSITY_DEFARGS  );

	
	/* 'Stg_Component' implementations */
	void* _ThreeLayeredViscosity_DefaultNew( Name name ) ;
	void _ThreeLayeredViscosity_AssignFromXML( void* rheology, Stg_ComponentFactory* cf, void* data );
   void _ThreeLayeredViscosity_Init( ThreeLayeredViscosity* self, FeMesh* feMesh, double eta0_1, double eta0_2, double eta0_3, Axis variationAxis, double referencePoint1, double referencePoint2);
   void _ThreeLayeredViscosity_Destroy( void* rheology, void* data ) ;
   
	void _ThreeLayeredViscosity_ModifyConstitutiveMatrix( 
		void*                                              rheology, 
		ConstitutiveMatrix*                                constitutiveMatrix,
		MaterialPointsSwarm*                               swarm,
		Element_LocalIndex                                 lElement_I,
		MaterialPoint*                                     materialPoint,
		Coord                                              xi );
#endif

